FROM python:3-alpine
ENV PYTHONUNBUFFERED=1

WORKDIR /django
RUN mkdir Data

COPY . /django
RUN pip install -r requirements.txt

RUN mkdir -p Data
VOLUME ["/Data"]

EXPOSE 8000
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000